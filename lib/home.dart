import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kiemtra/detail.dart';
import 'news.dart';
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'News',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage>
{
  List<String> myList = ['apple','tesla','google'];
  List<String> myList1 = ['https://image.thanhnien.vn/768/uploaded/nthanhluan/2019_03_26/applearcade_nhgo.jpg','https://metaqx.com/wp-content/uploads/2021/09/so-lieu-thong-ke-viet-nam-co-69-280-000-nguoi-su-dung-facebook.jpg','https://photo-cms-sggp.zadn.vn/Uploaded/2021/dufkxmeyxq/2021_10_08/googlebaonhieutuoi4_kamr.jpg'];
  CarouselController buttonCarouselController = CarouselController();
  late Future<List<Articles>> lsNew;
  static Future<List<Articles>> fetchData(String link) async{
    String url = "https://newsapi.org/v2/everything?q="+link+"&from=2021-10-16&sortBy=publishedAt&apiKey=d3f8405ae3f249dfa362d99de33a6377";
    var client = http.Client();
    var response = await client.get(Uri.parse(url));
    if (response.statusCode == 200) {
      var result = response.body;
      final user = newFromJson(result);
      return user.articles;
    }
    else
    {
      throw Exception("Lỗi lấy dữ liệu: ${response.statusCode}");
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    lsNew = fetchData("google");

  }
  void getTag(String link) {
    print(link);
    setState(() {
      lsNew = fetchData(link);
    });
  }
  void chuyen_mh(url){
      print(url);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => DetailScreen(url: url)),
      );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("News"),
        leading: Icon(Icons.line_style),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
          CarouselSlider.builder(
          itemCount: 3,
          carouselController: buttonCarouselController,
          itemBuilder: (BuildContext context, int itemIndex, int pageViewIndex){
            String p = myList[itemIndex];
            String img = myList1[itemIndex];
            print(img);
            return Container(
              height: 200,
              child: Column(
                children: [
                  GestureDetector(
                    onTap: ()
                    {
                      getTag(p);
                    },
                    child: Image.network(img,
                      height: 50,
                    ),
                  ),
                ],
              ),);
          }
          , options: CarouselOptions(
          height: 200,
          viewportFraction: 0.3,
          enableInfiniteScroll: true,
          autoPlayInterval: Duration(seconds: 1),
          autoPlayAnimationDuration: Duration(milliseconds: 800),
          autoPlayCurve: Curves.fastOutSlowIn,
          enlargeCenterPage: true,
          scrollDirection: Axis.horizontal,
        ),
        ),
          SizedBox(
              height: 400, // fixed height
              child: FutureBuilder(
                future: lsNew,
                builder: (BuildContext context, AsyncSnapshot<List<Articles>> snapshot) {
                  if(snapshot.hasData){
                    var data = snapshot.data as List<Articles>;

                    return ListView.builder(
                        itemCount: data.length,
                        itemBuilder:(BuildContext context, int index)
                        {
                          Articles a = data[index+1];
                          return Card(
                            child:
                            GestureDetector(
                                onTap: ()
                                {
                                  chuyen_mh(a.url);
                                },
                               child: Column(
                                 children: [
                                   Image.network(a.urlToImage,
                                     height: 200,),
                                   Text(a.title)
                                 ],
                               ),
                            )
                          );
                        });
                  }else{
                    return CircularProgressIndicator();
                  }
                },
              ),
            ),
          ],
      ),
    );
  }
}